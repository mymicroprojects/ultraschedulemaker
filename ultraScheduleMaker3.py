# Ultra Schedule Maker

import re
from pandas import DataFrame

def main():
	# Open the text file
	f = open("data/HORARIOS-2021-2.txt", encoding="utf-8", errors="ignore")
	archivoSalidaName = "exported/horarios_2021_1-LSC.xlsx"
	# Read all the lines as a string
	fullText = f.read();
	# Replace all bad characters

	corrections = {
		"Ú": "i",
		"µ": "o",
		"Ã": "a",
		"\\": "Ñ",
		"Õ": "e",
		"ê": "A"
	}

	for c in corrections:
		fullText = fullText.replace(c, corrections[c])

	# Get all the pages
	pages = re.findall("(UNIDAD ACADEMICA:(?:.+\n)+\n{2})", fullText)
	#print(pages[0])
	#print(len(pages))

	class_id = 0
	all_class_ids = []
	all_careers = []
	all_groups = []
	all_claves = []
	all_materias = []
	all_caps = []
	all_classTypes = []
	all_noControl = []
	all_profes = []
	all_edif = []
	all_salones = []
	all_subGroups = []

	all_lunes_start = []
	all_lunes_end = []
	all_martes_start = []
	all_martes_end = []
	all_miercoles_start = []
	all_miercoles_end = []
	all_jueves_start = []
	all_jueves_end = []
	all_viernes_start = []
	all_viernes_end = []
	all_sabado_start = []
	all_sabado_end = []
	all_domingo_start = []
	all_domingo_end = []

	calss_types = {"C": 0, "T": 0, "L": 0, "P": 0}

	clave = ""
	materia = ""
	profe = ""
	classType = ""

	# Get all class subClasss in each page
	for page in pages:
		#print("\n")

		p = Page(page)
		#p.showText()

		career = p.getCareer()
		#print("CARRERA: " + career)

		group = p.getGroup()
		#print("GRUPO: " + group)

		subClasses = p.getSubClasss()
		#print(subClasses[0].text)
		#print(subClasses[1].text)
		#print(len(subClasses))

		#print()

		for sc in subClasses:

			print(sc.getMateria() + " " + sc.getProfe())

			lastClave = clave
			clave = sc.getClave()
			#print("Clave:          " + clave)

			lastMateria = materia
			materia = sc.getMateria()
			#print("Materia:        " + materia)

			lastProfe = profe
			profe = sc.getProfe()
			#print("Professor:      " + profe)

			cap = sc.getCap()
			#print("Capacidad:      " + cap)

			lastClassType = classType
			classType = sc.getClassType()
			#print("Tipo de clase:  " + classType)

			'''if lastClassType == "P" and classType != "P":
				class_id += 1'''

			calss_types[classType] += 1
			# if the materia is not the same as the alst one, restart
			if not (materia == lastMateria and lastClave == clave):
				class_id += 1
				for ct in calss_types:
					calss_types[ct] = 0
				calss_types[classType] = 1
			# if it's a project, that's the only subClass
			elif calss_types["P"] >= 1:
				class_id += 1
				for ct in calss_types:
					calss_types[ct] = 0
			# if it's a class after having a T or a L, restart
			elif calss_types["C"] > 1 and classType == "C":
				if calss_types["T"] >= 1 or calss_types["L"] >= 1:
					class_id += 1
					for ct in calss_types:
						calss_types[ct] = 0
					calss_types["C"] = 1
			# if it's a T after having an L, restart
			'''elif calss_types["T"] > 1 and classType == "T":
				if calss_types["L"] > 0:
					class_id += 1
					for ct in calss_types:
						calss_types[ct] = 0
					calss_types["T"] = 1'''


			all_class_ids.append(class_id)

			noControl = sc.getNoControl()
			#print("No. de control: " + noControl)

			edif = sc.getEdif()
			#print("Edificio:       " + edif)

			salon = sc.getSalon()
			#print("Salón:          " + salon)
			
			subGroup = sc.getSubGroup()
			#print("SubGroup:       " + subGroup)

			all_careers.append(career)
			all_groups.append(group)
			all_claves.append(clave)
			all_materias.append(materia)
			all_caps.append(cap)
			all_classTypes.append(classType)
			all_noControl.append(noControl)
			all_profes.append(profe)
			all_edif.append(edif)
			all_salones.append(salon)
			all_subGroups.append(subGroup)

			sc.printMeetings()

			meetings = sc.getMeetings()
			for m in meetings:
				meeting = meetings[m].split(" - ")
				start = meeting[0]
				end = meeting[1]

				if m == "Lunes":
					all_lunes_start.append(start)
					all_lunes_end.append(end)
				elif m == "Martes":
					all_martes_start.append(start)
					all_martes_end.append(end)
				elif m == "Miercoles":
					all_miercoles_start.append(start)
					all_miercoles_end.append(end)
				elif m == "Jueves":
					all_jueves_start.append(start)
					all_jueves_end.append(end)
				elif m == "Viernes":
					all_viernes_start.append(start)
					all_viernes_end.append(end)
				elif m == "Sabado":
					all_sabado_start.append(start)
					all_sabado_end.append(end)
				elif m == "Domingo":
					all_domingo_start.append(start)
					all_domingo_end.append(end)

			#print()
			#break

		#break

	df = DataFrame({
		"ID": all_class_ids,
		"Carrera": all_careers,
		"Grupo": all_groups,
		"Clave": all_claves,
		"Materia": all_materias,
		"Capacidad": all_caps,
		"Tipo": all_classTypes,
		"No. Control": all_noControl,
		"Profesor(a)": all_profes,
		"Edif": all_edif,
		"Salón": all_salones,
		"SubGrupo": all_subGroups,
		"LunesI": all_lunes_start,
		"LunesF": all_lunes_end,
		"MartesI": all_martes_start,
		"MartesF": all_martes_end,
		"MiercolesI": all_miercoles_start,
		"MiercolesF": all_miercoles_end,
		"JuevesI": all_jueves_start,
		"JuevesF": all_jueves_end,
		"ViernesI": all_viernes_start,
		"ViernesF": all_viernes_end,
		"SabadoI": all_sabado_start,
		"SabadoF": all_sabado_end,
		"DomingoI": all_domingo_start,
		"DomingoF": all_domingo_end
		})

	df.to_excel(archivoSalidaName)

	print("Exported correctly!")

class Page:
	subClasses = []

	def __init__(self, text):
		self.text = text

	def showText(self):
		print(self.text)

	def getSubClasss(self):
		self.subClasses = []
		subClasss = re.findall("(-{3,}\n(?:[^-\n]+\n){3})", self.text)
		for subClass in subClasss:
			self.subClasses.append(SubClass(subClass))
		return self.subClasses

	def getCareer(self):
		self.career = re.search("CARRERA: \d+ +([()A-Z ]+)\n", self.text)
		return self.career.group(1)

	def getGroup(self):
		self.group = re.search("GRUPO: '(\d+)'", self.text)
		return self.group.group(1)

class SubClass:
	meetings = {}

	def __init__(self, text):
		self.text = text

	def getClave(self):
		self.clave = re.search("\d+", self.text)
		return self.clave.group()

	def getMateria(self):
		self.materia = re.search("[a-zA-Z](?:[a-zñA-ZÑ]|[a-zñA-ZÑ] )+[a-zA-Z]", self.text)
		return self.materia.group()

	def getCap(self):
		self.cap = re.search("[a-zA-Z](?:[a-zñA-ZÑ]|[a-zñA-ZÑ] )+[a-zA-Z] +(\d+)? +[CTLP] ", self.text)
		if not self.cap.group(1):
			return ""
		return self.cap.group(1)

	def getClassType(self):
		self.classType = re.search("[a-zA-Z](?:[a-zñA-ZÑ]|[a-zñA-ZÑ] )+[a-zA-Z] +(?:\d+)? +([CTLP]) ", self.text)
		return self.classType.group(1)

	def getNoControl(self):
		self.noControl = re.search("(?:[^\n]+\n){3}[^\d]*\d+ +(\d+)", self.text)
		return self.noControl.group(1)

	def getProfe(self):
		self.profe = re.search("(?:[^\n]+\n){3}[^\d]*\d+ +\d+ *([a-zA-Z*][a-zA-ZÑ.* ]+[a-zA-Z.*])", self.text)
		return self.profe.group(1)

	def getEdif(self):
		self.edif = re.search("(?:[^\n]+\n){3}[^\d]*\d+ +\d+ *[a-zA-Z*][a-zA-ZÑ.* ]+[a-zA-Z.*] *(\d)", self.text)
		return self.edif.group(1)

	def getSalon(self):
		self.salon = re.search("(?:[^\n]+\n){3}[^\d]*\d+ +\d+ *[a-zA-Z*][a-zA-ZÑ.* ]+[a-zA-Z.*] *\d *([\dTLABVIR]{3})", self.text)
		return self.salon.group(1)

	def getSubGroup(self):
		self.subGroup = re.search("(?:[^\n]+\n){3}[^\d]*\d+ +\d+ *[a-zA-Z*][a-zA-ZÑ.* ]+[a-zA-Z.*] *\d *[\dTLABVIR]{3}.{7}(\d| ) +\|", self.text)
		return self.subGroup.group(1)

	def getMeetings(self):
		self.meetingsData = re.search("[a-zA-Z](?:[a-zñA-ZÑ]|[a-zñA-ZÑ] )+[a-zA-Z] +(?:\d+)? +[CTLP] +\| +(?P<Lunes>(?:\d+:\d+| +)) \| +(?P<Martes>(?:\d+:\d+| +)) \| +(?P<Miercoles>(?:\d+:\d+| +)) \| +(?P<Jueves>(?:\d+:\d+| +)) \| +(?P<Viernes>(?:\d+:\d+| +)) \| +(?P<Sabado>(?:\d+:\d+| +)) \| +(?P<Domingo>(?:\d+:\d+| +)) ", self.text)

		self.meetings["Lunes"] = self.meetingsData.group("Lunes") + " - "
		self.meetings["Martes"] = self.meetingsData.group("Martes") + " - "
		self.meetings["Miercoles"] = self.meetingsData.group("Miercoles") + " - "
		self.meetings["Jueves"] = self.meetingsData.group("Jueves") + " - "
		self.meetings["Viernes"] = self.meetingsData.group("Viernes") + " - "
		self.meetings["Sabado"] = self.meetingsData.group("Sabado") + " - "
		self.meetings["Domingo"] = self.meetingsData.group("Domingo") + " - "

		self.meetingsData = re.search("(?:[^\n]+\n){3}[^\d]*\d+ +\d+ *[a-zA-Z*][a-zA-ZÑ.* ]+[a-zA-Z.*] *\d *[\dTLABVIR]{3}.{7}(\d| ) +\| +(?P<Lunes>(?:\d+:\d+| +)) \| +(?P<Martes>(?:\d+:\d+| +)) \| +(?P<Miercoles>(?:\d+:\d+| +)) \| +(?P<Jueves>(?:\d+:\d+| +)) \| +(?P<Viernes>(?:\d+:\d+| +)) \| +(?P<Sabado>(?:\d+:\d+| +)) \| +(?P<Domingo>(?:\d+:\d+| +)) ", self.text)
		self.meetings["Lunes"] += self.meetingsData.group("Lunes")
		self.meetings["Martes"] += self.meetingsData.group("Martes")
		self.meetings["Miercoles"] += self.meetingsData.group("Miercoles")
		self.meetings["Jueves"] += self.meetingsData.group("Jueves")
		self.meetings["Viernes"] += self.meetingsData.group("Viernes")
		self.meetings["Sabado"] += self.meetingsData.group("Sabado")
		self.meetings["Domingo"] += self.meetingsData.group("Domingo")

		return self.meetings

	def printMeetings(self):
		meetings = self.getMeetings()
		#print("\t DAY\t\t\tSTART\t\tEND")
		for m in meetings:
			meeting = meetings[m].split(" - ")
			#print("\t %s  \t\t%s\t\t%s" % (m, meeting[0], meeting[1]))

if __name__ == '__main__':
	main()