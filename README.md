# Description
Using **regex** with **python**, this script transforms my university-semestral-schedules **ugly-text format into an well-orginized excel file**, so that students may query and filter the subjects and schedule options available for them, provided with a better visual presentation.

## Note
The project data is in **spanish**, since it's the language of my city and university.

## Disclaimer
The data used and shown in this project is public and can be found here:
https://ingenieria.mxl.uabc.mx/index.php/horario-escolar-vigente

Each semester, the file is updated.

# Images
### The university-semestral-schedules ugly-text format looks like the following.
![](media/raw-text-preview.jpg)

### After the script runs, the generated excel result looks like the following.
![](media/result-preview.jpg)

### And after a little excel enhancement, I made it look like this:
![](media/enhanced-result-preview.jpg)
There are **2603** rows of subjects info.
I actually uploaded an enhanced version of the schedules to a facebook group so all students may have a better time building their schedules.
I did that for 3 semesters.

The **excel version** allows students to **filter** by attributes like career, subject or professor, and to **view the info of a subject in a more organized way**, making the task of building their schedule easier.

**Without the excel**, students would have to **manually search their subjects and schedules in the raw text** format, and although it has some order, its presentation is not quite practical for the purpose.

# Motivations
I entrusted myself this project since the first time I tried to build my schedule and saw the format the data was presented in. My mission was to make it easier for me and all the students to query and buil our schedules.

# Future updates
1. For the moment being, no updates are planned.
2. The script does not work with the 2022-1 version of the schedules posted by my university, since their format changed some key aspects.
